import { Component, OnInit } from "@angular/core";
import { PostRowDetails } from "../row.model";
import { rowsData } from "../rowsJsonData";
import { Http } from "@angular/http";

@Component({
  selector: "app-pagination",
  templateUrl: "./pagination.component.html",
  styleUrls: ["./pagination.component.css"]
})
export class PaginationComponent implements OnInit {
  data: any;
  headers: any;
  p = 1;
  numberOfColumns = 10;
  rowDetails: PostRowDetails;

  key = "name";
  reverse = false;
  sort(key) {
    this.key = key;
    this.reverse = !this.reverse;
  }

  constructor(private http: Http) {}
  ngOnInit(): void {
    this.rowDetails = new PostRowDetails();
    this.data = rowsData;
    this.headers = Object.keys(this.data[0]);
    /** convert all the lower case headers to upper case */
    for (let i = 0; i <= this.headers.length; i++) {
      if (this.headers[i]) {
        this.headers[i] = this.headers[i].toUpperCase();
      }
    }
  }
  /** construct query object to post using destructuring method */
  submitRow(row) {
    [this.rowDetails.id, this.rowDetails.status] = [row.id, row.status];
    this.postRows(this.rowDetails);
  }

  /** on key event press it display input number of results in table */
  onKey(event) {
    this.numberOfColumns = event.target.value ? event.target.value : 10;
  }

  /** method to post the query object and subscribe to the data */
  postRows(rowDetails) {
    this.http.post("url", rowDetails).subscribe(data => {
      data = rowDetails;
    });
  }
}
