import { Component } from "@angular/core";
import { Router } from "@angular/router";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent {
  constructor(private router: Router) {}

  /** navigate to different component based on argument */
  navigate = function(component) {
    this.router.navigateByUrl(`${component}`);
  };
}
