import { BrowserModule } from "@angular/platform-browser";
import {
  NgModule,
  CUSTOM_ELEMENTS_SCHEMA,
  NO_ERRORS_SCHEMA
} from "@angular/core";
import { KeysPipe } from "./keysPipe";

import { AppComponent } from "./app.component";
import { NgxPaginationModule } from "ngx-pagination";
import { HttpModule } from "@angular/http";
import { RouterModule, Routes } from "@angular/router";
import "reflect-metadata";
import { NoPaginationComponent } from "./no-pagination/no-pagination.component";
import { PaginationComponent } from "./pagination/pagination.component";

const appRoutes: Routes = [
  { path: "", redirectTo: "/pagination", pathMatch: "full" },
  {
    path: "pagination",
    component: PaginationComponent
  },
  { path: "no-pagination", component: NoPaginationComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    KeysPipe,
    NoPaginationComponent,
    PaginationComponent
  ],
  imports: [
    BrowserModule,
    NgxPaginationModule,
    HttpModule,
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true } // <-- debugging purposes only
    )
  ],
  providers: [],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA]
})
export class AppModule {}
